package com.products.backend.resources;

import java.util.List;

import com.products.backend.models.Produto;
import com.products.backend.repository.ProdutoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping(value="/api")
public class ProdutoResource {

    @Autowired
    ProdutoRepository produtoRepository;


    @GetMapping("/produtos")
    public List<Produto> listaProdutos() {
        return produtoRepository.findAll();
    }

    @GetMapping("/produto/{id}")
    public Produto listaProdutoUnico(@PathVariable(value="id") long id) {
        return produtoRepository.findById(id);
    }

    @PostMapping("/produto")
    @ResponseStatus(HttpStatus.CREATED)
    public Produto registraProduto(@RequestBody Produto produto) {
        return produtoRepository.save(produto);
    }

    @DeleteMapping("/produto/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletaProduto(@PathVariable(value="id") long id) {
        Produto produto = produtoRepository.findById(id);
        produtoRepository.delete(produto);
    }

    @PutMapping("/produto/{id}")
    public Produto atualizaProduto(@PathVariable(value="id") long id, @RequestBody Produto produto) {
        Produto produtoAtualizado = produtoRepository.findById(id);
        produtoAtualizado.setNome(produto.getNome());
        produtoAtualizado.setQuantidade(produto.getQuantidade());
        produtoAtualizado.setValor(produto.getValor());
        return produtoRepository.save(produtoAtualizado);
    }
}
